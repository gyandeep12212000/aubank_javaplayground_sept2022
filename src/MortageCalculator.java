import java.util.Scanner;
public class MortageCalculator {

	public static void main(String[] args) 
	{
        Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Principal Amount");
		int principalAmount=sc.nextInt();
		
		System.out.println("Enter Rate of Interest");
		float rateOfIntrst=sc.nextFloat();
		
		System.out.println("Enter time period (in years) ");
		int timePeriod=sc.nextInt();
	  
	    float monthlyIntrst=rateOfIntrst/(100*12);
	    
		int numberOfPayments=timePeriod*12;
		
		double mortage=principalAmount*(monthlyIntrst*Math.pow(1+monthlyIntrst, numberOfPayments))/(Math.pow(1+monthlyIntrst,numberOfPayments)-1);
		System.out.println("Calculated Mortage Amount is : "+mortage);

	}

}
