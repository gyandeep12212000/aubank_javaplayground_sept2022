
public class ObjectCounterMain {

	public static void main(String[] args) {
		ObjectCounter oc1,oc2,oc3;
		
		oc1=new ObjectCounter();
		oc2=new ObjectCounter();
		oc3=new ObjectCounter();   
	
        oc1.increment();
        oc2.increment(); 
        oc3.increment(); 
	} 

}
