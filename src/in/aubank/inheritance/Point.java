package in.aubank.inheritance;

import java.util.Objects;

public class Point {

	int x;
	int y;
	
	Point(int x,int y){
		this.x=x;
		this.y=y;
	}
	public boolean equals(Object otherObject) {
		Point other=(Point)otherObject;
		return (this.x==other.x)&&(this.y==other.y);
	}
	@Override
	public int hashCode() {
		return Objects.hash(this.x,this.y);
	}
	
	@Override
	public String toString() {
		String representation="Point x: "+this.x+"|y: "+this.y;
		return representation;
	}
    
}
