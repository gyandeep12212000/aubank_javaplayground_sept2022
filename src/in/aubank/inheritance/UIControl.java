package in.aubank.inheritance;

public abstract class UIControl {
	boolean enabled=true;
	
	public UIControl(boolean enabled) {
		this.enabled=enabled;
	}
	public void enable() {
		enabled=true;
	}

	public void disable() {
		enabled=false;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public abstract void render();
	
}
