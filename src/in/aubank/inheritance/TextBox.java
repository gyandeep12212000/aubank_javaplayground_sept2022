package in.aubank.inheritance;

public class TextBox extends UIControl{
	String text;
	
//	public IconifiedTextBox() {
//		super(true);
//	}
	
	public TextBox(boolean enabled) {
		super(enabled);
	}
	
	public void setText(String text) {
		this.text=text;
	}
	
	public String getText() {
		return text;
	}
	
	public void clear() {
		text="";
	}
	
}
