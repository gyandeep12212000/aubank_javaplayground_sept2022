package in.aubank.interfaces;

public class TaxReport {
  
	//tight coupling
	//TaxCalculator calculator=new TaxCalculator2020(0);
	TaxCalculator calculator;
	
	public TaxReport(TaxCalculator calculator) {
		this.calculator=calculator;
	}
	
	public void show() {
	    double tax=calculator.calculateTax();
		System.out.println("Tax on your income of 100000 is "+tax);
	}
}
