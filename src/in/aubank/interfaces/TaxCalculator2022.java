package in.aubank.interfaces;

public class TaxCalculator2022 implements TaxCalculator{
    
	double taxableIncome;
    
	public TaxCalculator2022(double taxableIncome) {
    	this.taxableIncome=taxableIncome;
    	  }
	
	@Override
    public double calculateTax(){
        return taxableIncome*0.40;
        }
}