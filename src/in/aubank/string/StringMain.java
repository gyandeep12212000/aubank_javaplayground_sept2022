package in.aubank.string;

public class StringMain {

	public static void main(String[] args) {
		
		String s1="AU";
		String s2="Small Finance Bank";
		
		System.out.println("--------------Concat----------");
		String s3=s1.concat(s2);
		System.out.println(s3);
		
		String s4=s1+s2;
		System.out.println(s4);
		
		System.out.println("------Length--------");
		int l=s4.length();
		System.out.println("Length of string "+s4+" is "+l);
		
		System.out.println("-------Equality---------");
		
		String s5="AuBank";
		String s6="aubank";
		
		System.out.println("Equals: "+s5.equals(s6));
		System.out.println("Equals Ignore Case: "+s5.equalsIgnoreCase(s6));

		System.out.println("---------Trims and empty checks----------");
		String s7="     Au     Bank    ";
		System.out.println("Length before trimming : "+s7.length());
		s7=s7.trim();
		System.out.println("Length after trimming : "+s7.length());
		
		String s8="";
		String s9=" ";
		System.out.println("Empty check for s8 : "+s8.isEmpty());
		System.out.println("Empty check for s9 : "+s9.isEmpty());
	
	    System.out.println("-----------Index and char------");
	    String s10="Finance";
	    System.out.println("Charcter at index 2 is :"+s10.charAt(2));
       	System.out.println("Index of first n is :"+s10.indexOf("n"));
	   

        int indexOfFirstN=s10.indexOf("n");
        int indexOfSecondN=s10.indexOf("n",indexOfFirstN+1);
        System.out.println("Index of Second n :"+indexOfSecondN);
	
	    System.out.println("---------Upper Case and Lower case---------");
	    String s11="au bank";
	    String s12="AU BANK";
	    
	    System.out.println(s11+"--"+s11.toUpperCase());
	    System.out.println(s12+"--"+s12.toLowerCase());
	    
	    String line="1,Tom,IT,350000";
	    System.out.println(line);
	    String[] data=line.split(",");
	    
	    for(String d:data) {
	    	System.out.println(d);
	    }
	    
	    String s13="Jxvc";
	    System.out.println(s13);
	    s13=s13.replace("c","a");
	    System.out.println(s13);
        
	    System.out.println("------Sub String---------");
	    String word="Hamburger";
	    System.out.println(word.substring(3));
	    System.out.println(word.substring(4,8));
	    
	}

}
