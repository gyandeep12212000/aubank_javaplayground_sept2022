package in.aunbank.exceptions;

public class Voter {
    
    String name,gender;
    int age;
    final String nationality="Indian";
    
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        
        if(age>=18 && age<=120) {
        this.age = age;
        }
        
        else {
            throw new IllegalStateException("Age should be between 18 and 120");
        }
    }
    
    public void show() {
        System.out.println("Name : "+name);
        System.out.println("Gender : "+gender);
        System.out.println("Age : "+age);
        
    }
    
    
}

//// Create a class Voter
//// Voter have 4 members -- name,gender,age,nationality
//// Voter will enter the values using console
//// Allowed range of voting is between 18 and 120
//// Create a custom exception VoterException, triggered if wrong age is entered