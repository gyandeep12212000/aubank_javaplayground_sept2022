package in.aunbank.exceptions;

import java.io.FileReader;
import java.io.IOException;

/**
 * @author 296123
 *
 */
public class ExceptionsMain {

	public static void main(String[] args) {
		FileReader fr=null;
		try {
		int[] numbers= {1,2,3,4,5};
		System.out.println(numbers[2]);
//		System.out.println(numbers[8]);
		int i=10/10;
	    fr=new FileReader("D:\\abc.txt");
		fr.close();
		System.out.println(numbers[8]);
		}
		
		catch(ArithmeticException e) {
			System.out.println("----Arith Excep-----");
			e.printStackTrace();
			
		}
		
		catch(IOException e) {
			System.out.println("-----IO Except------");
			e.printStackTrace();
		} 
		
		
		catch(Exception e) {
			System.out.println("Generic Exception");
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		finally {
			System.out.println("Executes when no except occur");
			System.out.println("Executes when except occure");
			try{ 
				fr.close();
			System.out.println("File reader not closed");
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		} 
		
		System.out.println("Exception reaches here -----");
	   
	    }
}


