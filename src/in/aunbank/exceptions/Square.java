package in.aunbank.exceptions;

public class Square {

	int length;
	public void setLength(int length) throws Exception {
		if(length>0) {
		this.length=length;}
		else
		{
			throw new IllegalStateException("Length should be greater than 0");
		}
		
	}

	public void area() {
		int area= length*length;
		System.out.println("Area of square of "+length+" is "+area);
	}
	
}
