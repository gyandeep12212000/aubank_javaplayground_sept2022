package in.aunbank.exceptions;

public class CircleArrayMain {
	
	public static void main(String[] args) {
		
		Circle[] cArray;
		cArray=new Circle[3];
		cArray[1]=new Circle(25);
		
		Circle myCircle = new Circle(34);
		cArray[2]=myCircle;
		
		cArray[0]=cArray[1];
		
		for(Circle c:cArray) {
			System.out.print(c);
		}
	}
}