package in.aunbank.exceptions;

public class SquareException extends Exception {
	
	public SquareException(String message) {
		super(message);
	}

}
