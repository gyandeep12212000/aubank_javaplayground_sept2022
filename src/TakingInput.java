import java.util.Scanner;
public class TakingInput {

	public static void main(String[] args)
	{   
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Name");
		String name=sc.next();
		
		System.out.println("Enter age");
		int age=sc.nextInt();
		
		System.out.println("Enter temp");
		float temp=sc.nextFloat();
	  
	    sc.close();
	}

}
