//Write a class employee
//It has 3 members[name,base salary,hourly rates]
//Write a method for calculating the wages
//Note: Some employees do not have the extra hours permission
//
//Whenever an employee object is created , maintain a variable
//to count the total no. of employees

//Following are employees
//Tom,12000,50
//Alex,10000,45
//Mike,12500,50
//John, 13000,0


//public class Employee {
//	static int count=0;
//	
//	String name;
//	int baseSalary;
//	int hourlyRates;
//	int numberOfHours;
//	Employee(String name,int baseSalary, int hourlyRates,int numberOfHours){
//		this.name=name;
//		this.baseSalary=baseSalary;
//		this.hourlyRates=hourlyRates;
//		this.numberOfHours=numberOfHours;
//		count++;
//	}
//	
//	Employee E1=new Employee("Tom",12000,50,4);
//    Employee E2=new Employee("Alex",10000,45,5);
//    Employee E3=new Employee("Mike",12500,50,6);
//    Employee E4=new Employee("John",13000,0,7);
//    
//    public void wages() {
//    	if(hourlyRates!=0) {
//    		float wage;
//    		wage=(baseSalary+hourlyRates/numberOfHours);
//    		System.out.println("Wage of "+name+"is"+ wage);
//    		
//    	}
//    	
//}
//}
public class Employee{
	
	
	private String name;
	private int baseSalary;
	private int hourlyRate;
	
	Employee(String name,int baseSalary){
		this(name,baseSalary,0);
	}
	
	Employee(String name,int baseSalary,int hourlyRate){
		setName(name);
		setBaseSalary(baseSalary);
		setHourlyRate(hourlyRate);
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setBaseSalary(int baseSalary) {
		if(baseSalary<=0) {
			throw new IllegalStateException("Base Salary is supposed to greater than 0");
		}
		else
		{
			this.baseSalary=baseSalary;
		}
	}
	
	public int getBaseSalary() {
		return baseSalary;
	}
	
	public void setHourlyRate(int hourlyRate) {
		if(hourlyRate<0)
		{
		throw new IllegalStateException("Hourly Rate has to be grater than 0");
		}
		else
		{
			this.hourlyRate=hourlyRate;
		}
	}
	
	public int calculateWage(int numberOfHours) {
	   int wage=baseSalary+(numberOfHours*hourlyRate); 
       return wage;	
	}
}

//x
//
//public void setX(Type x) {
//	this.x=x;
//}
//
//public void getX(Type x) {
//	this.x=x;
//}