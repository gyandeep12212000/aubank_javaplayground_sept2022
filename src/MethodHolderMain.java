
public class MethodHolderMain {

	public static void main(String[] args) {
		MethodHolder mh=new MethodHolder();
		mh.display();
		mh.add(10,20);
		int gst=mh.getGST();
		System.out.println("GST Rate : "+gst);
		int squaredValue=mh.square(7);
		System.out.println("Squared Value : "+squaredValue);
	}
}
